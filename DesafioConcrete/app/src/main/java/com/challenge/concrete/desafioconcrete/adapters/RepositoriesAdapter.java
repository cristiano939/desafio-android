package com.challenge.concrete.desafioconcrete.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.challenge.concrete.desafioconcrete.R;
import com.challenge.concrete.desafioconcrete.models.Repository;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by User on 12/01/2017.
 */
public class RepositoriesAdapter extends ArrayAdapter<Repository> {

    public RepositoriesAdapter(Context ctx, List<Repository> repositoryList) {
        super(ctx, R.layout.repository_list_item_layout);
        this.repositoryList = repositoryList;

    }

    public List<Repository> repositoryList;
    public Context ctx;

    @Override
    public int getCount() {
        return repositoryList.size();
    }

    @Override
    public Repository getItem(int position) {
        return repositoryList.get(position);
    }

    public void addRepos(List<Repository> repos) {
        repositoryList.addAll(repos);
        super.addAll(repos);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Repository repo = getItem(position);
        convertView = LayoutInflater.from(getContext()).inflate(R.layout.repository_list_item_layout, parent, false);
        TextView repoName = (TextView) convertView.findViewById(R.id.repository_name);
        TextView repoDescription = (TextView) convertView.findViewById(R.id.repository_description);
        TextView prAmount = (TextView) convertView.findViewById(R.id.repository_pr_amount);
        TextView starAmount = (TextView) convertView.findViewById(R.id.repository_star_amount);
        TextView ownerUserName = (TextView) convertView.findViewById(R.id.repository_owner_username);
        TextView ownerFullName = (TextView) convertView.findViewById(R.id.repository_owner_fullname);
        ImageView avatar = (ImageView) convertView.findViewById(R.id.user_avatar);

        repoName.setText(repo.name);
        repoDescription.setText(repo.description);
        prAmount.setText(repo.forks);
        starAmount.setText(repo.starGazers);
        ownerUserName.setText(repo.owner.login);
        ownerFullName.setText(repo.owner.name);

        Picasso.with(getContext()).load(repo.owner.avatar_url).into(avatar);

        return convertView;
    }
}
