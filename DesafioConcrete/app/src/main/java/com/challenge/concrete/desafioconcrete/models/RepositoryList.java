package com.challenge.concrete.desafioconcrete.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by User on 15/01/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RepositoryList {

    @JsonProperty("total_count")
    public int count;
    @JsonProperty("items")
    public List<Repository> repositoryList;
}
