package com.challenge.concrete.desafioconcrete.clients;

import com.challenge.concrete.desafioconcrete.models.Owner;
import com.challenge.concrete.desafioconcrete.models.Pulls;
import com.challenge.concrete.desafioconcrete.models.RepositoryList;

import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Path;
import org.androidannotations.rest.spring.annotations.Post;
import org.androidannotations.rest.spring.annotations.Rest;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.List;

/**
 * Created by User on 12/01/2017.
 */
@Rest(rootUrl = "https://api.github.com", converters = { MappingJackson2HttpMessageConverter.class })
public interface GitHubClient {

    @Get("/search/repositories?q=language:Java&sort=stars&page={page}?client_id=8bbf79a9e472554f6ede&client_secret=1d919e9b61800bc10e8891b1b51b334e912012af")
    public RepositoryList GetRepositories(@Path int page);


    @Get("/users/{username}?client_id=8bbf79a9e472554f6ede&client_secret=1d919e9b61800bc10e8891b1b51b334e912012af")
    public Owner GetUser(@Path String username);

    @Get("/repos/{username}/{repository}/pulls?client_id=8bbf79a9e472554f6ede&client_secret=1d919e9b61800bc10e8891b1b51b334e912012af")
    public List<Pulls> GetRepositoryPulls(@Path String username, @Path String repository);
}

//https://api.github.com/users/ReactiveX
//https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1
//https://api.github.com/repos/<criador>/<repositório>/pulls