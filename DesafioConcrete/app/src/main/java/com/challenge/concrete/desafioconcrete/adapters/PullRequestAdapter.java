package com.challenge.concrete.desafioconcrete.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.challenge.concrete.desafioconcrete.R;
import com.challenge.concrete.desafioconcrete.models.Pulls;
import com.challenge.concrete.desafioconcrete.models.Repository;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by User on 15/01/2017.
 */
public class PullRequestAdapter extends ArrayAdapter<Pulls> {


    public List<Pulls> pullList;
    public Context ctx;


    public PullRequestAdapter(Context ctx, List<Pulls> pullList) {
        super(ctx, R.layout.pull_request_list_item);
        this.pullList = pullList;
    }

    @Override
    public int getCount() {
        return pullList.size();
    }

    @Override
    public Pulls getItem(int position) {
        return pullList.get(position);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Pulls pull = getItem(position);
        convertView = LayoutInflater.from(getContext()).inflate(R.layout.pull_request_list_item, parent, false);
        TextView title = (TextView) convertView.findViewById(R.id.pull_request_title);
        TextView username = (TextView) convertView.findViewById(R.id.pull_request_username);
        TextView fullName = (TextView) convertView.findViewById(R.id.pull_request_fullname);
        TextView body = (TextView) convertView.findViewById(R.id.pull_request_body);
        ImageView avatar = (ImageView) convertView.findViewById(R.id.pull_request_useravatar);

        title.setText(pull.title);
        username.setText(pull.owner.login);
        fullName.setText(pull.owner.name);
        body.setText(pull.description);

        Picasso.with(getContext()).load(pull.owner.avatar_url).into(avatar);

        return convertView;
    }
}
