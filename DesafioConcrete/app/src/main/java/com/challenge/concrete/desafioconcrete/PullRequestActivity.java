package com.challenge.concrete.desafioconcrete;

import android.app.ActionBar;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.challenge.concrete.desafioconcrete.adapters.PullRequestAdapter;
import com.challenge.concrete.desafioconcrete.adapters.RepositoriesAdapter;
import com.challenge.concrete.desafioconcrete.clients.GitHubClient;
import com.challenge.concrete.desafioconcrete.models.Pulls;
import com.challenge.concrete.desafioconcrete.models.Repository;
import com.challenge.concrete.desafioconcrete.models.RepositoryList;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.WindowFeature;
import org.androidannotations.rest.spring.annotations.RestService;

import java.util.Date;
import java.util.List;

@EActivity(R.layout.activity_pull_request)
public class PullRequestActivity extends AppCompatActivity {

    @ViewById(R.id.pr_listview)
    ListView prList;

    PullRequestAdapter adapter;

    @ViewById(R.id.pr_loading_layout)
    View loadingLayout;

    @RestService
    GitHubClient _client;

    @Extra("username")
    String userName;
    @Extra("reponame")
    String repoName;

    List<Pulls> pulls;

    @AfterInject
    public void loadData() {
        try {
            GetPullsData();
        } catch (Exception ex) {
            Log.e("Error getting data", ex.toString());
        }
    }



    @Background
    public void GetPullsData() {
        try {

            pulls = _client.GetRepositoryPulls(userName, repoName);
            if (pulls != null) {
                for (Pulls pull : pulls) {
                    try {
                        pull.owner = _client.GetUser(pull.owner.login);
                    } catch (Exception ex) {
                        pull.owner.name = "OAuth Issue";
                    }
                }
                ShowList();
            }

        } catch (Exception ex) {

            Log.e("Error getting repo", ex.toString());
        }
    }


    @UiThread
    public void ShowList() {
        adapter = new PullRequestAdapter(this, pulls);
        prList.setAdapter(adapter);
        prList.setVisibility(View.VISIBLE);
        prList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pulls pull = adapter.getItem(position);
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pull.url));
                startActivity(browserIntent);
            }
        });
        loadingLayout.setVisibility(View.GONE);
    }
}