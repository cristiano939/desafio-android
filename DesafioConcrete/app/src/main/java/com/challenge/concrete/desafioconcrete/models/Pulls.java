package com.challenge.concrete.desafioconcrete.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by User on 15/01/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Pulls {
    @JsonProperty("title")
    public String title;
    @JsonProperty("created_at")
    public String date;
    @JsonProperty("body")
    public String description;
    @JsonProperty("user")
    public Owner owner;
    @JsonProperty("html_url")
    public String url;
}




