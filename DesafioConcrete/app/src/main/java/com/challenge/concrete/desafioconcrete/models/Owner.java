package com.challenge.concrete.desafioconcrete.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by User on 12/01/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Owner {
   @JsonProperty("login")
   public String login;
    @JsonProperty("avatar_url")
    public String avatar_url;
    @JsonProperty("name")
    public String name;
    @JsonProperty("url")
    public String url;

    public Owner() {
    }

    public Owner(String login, String avatarUrl, String name) {
        this.login = login;
        this.avatar_url = avatarUrl;
        this.name = name;
    }

}


