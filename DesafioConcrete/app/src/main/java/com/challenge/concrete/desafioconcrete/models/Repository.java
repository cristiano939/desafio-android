package com.challenge.concrete.desafioconcrete.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by User on 12/01/2017.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Repository {

    @JsonProperty("id")
    public int id;
    @JsonProperty("description")
    public String description;
    @JsonProperty("name")
    public  String name;
    @JsonProperty("owner")
    public  Owner owner;
    @JsonProperty("forks")
    public  String forks;
    @JsonProperty("stargazers_count")
    public  String starGazers;

    public Repository() {

    }

    public Repository(int id, String description, String name, String forks, String starGazers) {
        this.id = id;
        this.description = description;
        this.name = name;

        this.forks = forks;
        this.starGazers = starGazers;
    }
}
