package com.challenge.concrete.desafioconcrete;

import android.app.ActionBar;
import android.graphics.Color;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.challenge.concrete.desafioconcrete.adapters.RepositoriesAdapter;
import com.challenge.concrete.desafioconcrete.clients.GitHubClient;
import com.challenge.concrete.desafioconcrete.models.Owner;
import com.challenge.concrete.desafioconcrete.models.Pulls;
import com.challenge.concrete.desafioconcrete.models.Repository;
import com.challenge.concrete.desafioconcrete.models.RepositoryList;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.ViewsById;
import org.androidannotations.annotations.WindowFeature;
import org.androidannotations.rest.spring.annotations.RestService;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.List;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {


    RepositoriesAdapter adapter;
    int repoCount = 1;

    @ViewById(R.id.repositorieslistview)
    ListView repoList;

    @ViewById(R.id.load_more_repos)
    TextView seeMoreText;

    @ViewById(R.id._load_more_progress_wheel)
    View loadMoreWheel;

    @ViewById(R.id.loadinglayout)
    View loadingLayout;

    @ViewById(R.id.load_more_button)
    LinearLayout loadMoreButton;

    @RestService
    GitHubClient _client;


    RepositoryList repositoryList;



    @AfterInject
    public void loadData() {
        try {

            GetRepositoryData();
        } catch (Exception ex) {
            Log.e("Error getting data", ex.toString());
        }
    }

    @Click(R.id.load_more_button)
    public void LoadMoreRepos() {
        GetMoreRepos();
    }


    @Background
    public void GetRepositoryData() {
        try {
            Log.d("Start Time", new Date().toString());
            repositoryList = _client.GetRepositories(repoCount);
            repoCount++;
            if (repositoryList != null) {
                for (Repository repo : repositoryList.repositoryList) {
                    try {
                        repo.owner = _client.GetUser(repo.owner.login);
                    } catch (Exception ex) {
                        repo.owner.name = "OAuth Issue";
                    }
                }
                ShowList();
            }
            Log.d("End Time", new Date().toString());

        } catch (Exception ex) {

            Log.e("Error getting repo", ex.toString());
        }
    }

    @Background
    public void GetMoreRepos() {
        ToggleMoreReposLoading();
        RepositoryList repoList = _client.GetRepositories(repoCount);
        repoCount++;
        if (repoList != null) {
            for (Repository repo : repoList.repositoryList) {
                try {
                    repo.owner = _client.GetUser(repo.owner.login);
                } catch (Exception ex) {
                    repo.owner.name = "OAuth Issue";
                }
            }
        }
        AddMoreRepos(repoList.repositoryList);

    }

    @UiThread
    public void AddMoreRepos(List<Repository> repolist) {
        adapter.addRepos(repolist);
        adapter.notifyDataSetChanged();
        ToggleMoreReposLoading();
    }

    @UiThread
    public void ToggleMoreReposLoading() {
        if (loadMoreWheel.getVisibility() == View.GONE) {
            seeMoreText.setVisibility(View.GONE);
            loadMoreWheel.setVisibility(View.VISIBLE);
        } else {
            seeMoreText.setVisibility(View.VISIBLE);
            loadMoreWheel.setVisibility(View.GONE);
        }
    }

    @UiThread
    public void ShowList() {
        adapter = new RepositoriesAdapter(this, repositoryList.repositoryList);
        repoList.setAdapter(adapter);
        repoList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (adapter.getCount() == repoList.getLastVisiblePosition()
                        || adapter.getCount() - 1 == repoList.getLastVisiblePosition()
                        || adapter.getCount() - 2 == repoList.getLastVisiblePosition()
                        || adapter.getCount() - 3 == repoList.getLastVisiblePosition()) {
                    loadMoreButton.setVisibility(View.VISIBLE);
                } else {
                    loadMoreButton.setVisibility(View.GONE);
                }
            }
        });
        repoList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Repository repo = adapter.getItem(position);
                PullRequestActivity_.intent(MainActivity.this).extra("username", repo.owner.login).extra("reponame", repo.name).start();
            }
        });
        repoList.setVisibility(View.VISIBLE);
        loadingLayout.setVisibility(View.GONE);
    }
}
