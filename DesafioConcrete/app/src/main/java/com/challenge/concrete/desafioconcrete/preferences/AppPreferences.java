package com.challenge.concrete.desafioconcrete.preferences;

import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by User on 15/01/2017.
 */
@SharedPref(SharedPref.Scope.UNIQUE)
public interface AppPreferences {
   String gitHubToken();
}
